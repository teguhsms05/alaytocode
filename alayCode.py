""" Saya ingin membuat kode untuk kata alay, dengan algoritma ini:

1. Ambil huruf pertama dari kata
2. Ganti huruf selanjutnya dengan nilai angka pada tabel
3. Jika terdapat huruf yang lebih dari 1 kali, ambil 1 huruf saja dan abaikan huruf lain
4. Abaikan huruf dengan nilai angka nol (0)
5. Stop mengganti huruf dengan angka sampai 3 angka saja
6. Ambil huruf terakhir dari kata
7. Gabungkan huruf pertama + 3 angka + huruf terakhir menjadi 1 kode
"""

import csv, shutil
from tempfile import NamedTemporaryFile

zero = ['a','e','i','o','u','h','w','y']
one = ['b','f','p','v']
two = ['c','g','j','k','q','r','s','x','z']
three = ['d','t']
four = ['l']
five = ['n','m']
six = ['r']
library = {'zero':zero, 'one':one, 'two':two, 'three':three, 'four':four, 'five':five, 'six':six}

# funct check character
def check_char(arg):
    key_dic = ''
    for key,val in library.items():
        if arg in val:
            key_dic = key
    return key_dic

# function change string to number
def string_to_number(arg): 
    switcher = {
        "zero": 0,
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6
	}
    return switcher.get(arg, "nothing") 

# function process Kata
def processKata(kata):
    first_last = ''
    temp_kata = ''
    result = ''
    for i in kata:
        if i not in temp_kata:
            temp_kata+=i
    count_result = len(temp_kata)
    first_last = temp_kata[:1].capitalize()+temp_kata[-1:].capitalize()
    for i in range(1,count_result):
        key_dic = check_char(temp_kata[i])
        rpl_val = string_to_number(key_dic)
        if rpl_val != 0: result+=str(rpl_val) 
    if len(result) == 2: result+='0'
    last_result = first_last[0]+result+first_last[1]
    return (last_result)
    
def main():
    tempfile = NamedTemporaryFile(mode='w', delete=False)
    filename = 'Kamus Alay.csv'
    with open(filename, 'r') as read_file, tempfile:
        reader = csv.DictReader(read_file)
        header = reader.fieldnames
        writer = csv.DictWriter(tempfile, fieldnames=header)
        
        data_reader = list(reader)
        soundex = [processKata(i.get('kata alay')) for i in data_reader]
        writer.writeheader()
        try:
            
            [writer.writerow({header[0]: row[header[0]], header[1]: soundex[i]}) for i, row in enumerate(data_reader)]
            print ('parsing success')
        except Exception as err:
            print (err)
    shutil.move(tempfile.name, filename)
            
if __name__ == '__main__':
    main()
    
